KindEditor.plugin('xiumi', function (K) {
    var self = this, name = 'xiumi';
    self.clickToolbar(name, function () {
        var html = ['<div style="padding:10px 20px;">',
            '<div class="ke-xiumi" style="width:1200px;height:800px;"></div>',
            '</div>'].join('');
        var dialog = self.createDialog({
            name: name,
            width: 1200,
            title: self.lang(name),
            body: html
        });
        var div = dialog.div,
            url = self.pluginsPath + 'xiumi/xiumi.html';
        var iframe = K('<iframe id="xiumi-iframe" frameborder="0" src="' + url + '" style="width:100%;height:800px;"></iframe>');
        console.log('window', self);
        iframe.bind('load', function () {
            win = iframe[0].contentWindow;
            win.setEdit(self);
        });
        K('.ke-xiumi', div).replaceWith(iframe);
    });
});
